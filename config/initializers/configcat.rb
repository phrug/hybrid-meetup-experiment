require 'configcat'
require 'singleton'

class FeatureFlagConfig
  include Singleton
  def initialize
    @configcat_client = ConfigCat.get(
    	'configcat-sdk-1/KobcCFZRa0GLvAk3k59CfQ/WiSHizDwVUSv59RZmBivMQ' # <-- This is the actual SDK Key for your Test Environment environment.
	)
  end

  def get_value(flag)
  	@configcat_client.get_value(flag, false)
  end

  def all_values
    @configcat_client.get_all_values
  end

  def refresh
    @configcat_client.force_refresh
  end

  def config
    ConfigCat::FetchResponse.initialize.json
  end
end