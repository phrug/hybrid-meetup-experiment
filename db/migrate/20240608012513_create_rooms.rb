class CreateRooms < ActiveRecord::Migration[7.1]
  def change
    create_table :rooms do |t|
      t.references :user, null: false, foreign_key: true
      t.string :uuid
      t.string :name
      t.datetime :scheduled_time

      t.timestamps
    end
  end
end
