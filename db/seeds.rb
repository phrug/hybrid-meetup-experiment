require 'factory_bot_rails'

include FactoryBot::Syntax::Methods

org = create(:organization, name: 'PhRUG')
admin = create(:user, email: 'superman@example.com', password: 'awesomest')
create(:membership, user: admin, organization: org)
admin.add_role(:admin)

user = create(:user, email: 'manny@example.com', password: 'awesomest')
create(:membership, user: user, organization: org)

create(:room, user: admin)
