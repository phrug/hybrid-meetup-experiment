FROM ruby:3.2.2-bookworm

LABEL maintainer="al@paglalayag.net"

RUN (curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -) && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

RUN apt-get update

RUN apt-get install -yq chromium firefox-esr libvips yarn

RUN wget https://github.com/mozilla/geckodriver/releases/download/v0.32.0/geckodriver-v0.32.0-linux64.tar.gz
RUN tar -xvzf geckodriver-v0.32.0-linux64.tar.gz
RUN rm geckodriver-v0.32.0-linux64.tar.gz
RUN chmod +x geckodriver
RUN cp geckodriver /usr/local/bin/

RUN wget https://chromedriver.storage.googleapis.com/90.0.4430.24/chromedriver_linux64.zip && unzip chromedriver_linux64.zip

RUN mv chromedriver /usr/bin/chromedriver
RUN chown root:root /usr/bin/chromedriver
RUN chmod +x /usr/bin/chromedriver

WORKDIR /usr/src/app

# Application dependencies
COPY Gemfile* /usr/src/app/

RUN gem install bundler
RUN bundle install

# Install nodejs dependencies
COPY package.json yarn.lock ./
RUN curl -fsSL https://deb.nodesource.com/setup_20.x -o nodesource_setup.sh
RUN bash nodesource_setup.sh
RUN yarn install --frozen-lockfile

ENV PORT=3000
ENV PORT2=4000
EXPOSE $PORT
EXPOSE $PORT2

# Copy application code to the container image
COPY . /usr/src/app

RUN yarn build:css

COPY config/credentials-cloudtest.yml.enc /usr/src/app/config/credentials.yml.enc 

ENV DOCKER=true
ENV RAILS_ENV=test
ENV RAILS_SERVE_STATIC_FILES=true

CMD ["bin/rails", "server", "-b", "0.0.0.0"]