require 'rails_helper'
require_relative '../helpers/authentication_helpers'

RSpec.describe "managing and entering rooms" do
	let(:phrug) { create(:phrug) }
	let(:admin) { create(:admin) }
	let(:room) { create(:room, user: admin) }

	context "an unauthenticated user" do	
		it "views no room text on signin" do
			phrug
			visit rooms_path
			expect(page).to have_text(I18n.t("rooms.index.empty"))
		end
	end

	context "an authenticated user" do
	end

	context "an admin" do
		before do
		    create(:membership, user: admin, organization: phrug)
			sign_in_as admin
		end

		it "views no room text on signin" do
			visit rooms_path
			expect(page).to have_text(I18n.t("rooms.index.empty"))
		end

		it "Creating a new room" do
			visit rooms_path
	
			click_on I18n.t("rooms.index.new_button")

			fill_in "Name", with: "Ruminating Room"
			click_on I18n.t("rooms.form.submit")

			expect(page).to have_text("Ruminating Room")

			visit rooms_path
			expect(page).to have_selector("h1", :text => "#{phrug.name}'s Rooms")
		end

		it "Showing a room" do
			room
			visit rooms_path
			click_link room.name

			expect(page).to have_button 'Enter'
			expect(page).to have_text room.name
		end

		it "Updating a room" do
			room
			visit rooms_path

			click_on "Edit", match: :first

			fill_in "Name", with: "Updated room"
			click_on "Submit"

			expect(page).to have_selector("h1", :text => "Rooms")
			expect(page).to have_text "Updated room"
		end

		it "Destroying a room" do
			room
			visit rooms_path
			expect(page).to have_text room.name

			click_on "Delete", match: :first
			expect(page).to_not have_text room.name
		end
	end
end
