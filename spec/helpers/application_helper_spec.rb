require 'rails_helper'

RSpec.describe ApplicationHelper do
	it "formats page specific title" do
		content_for(:title) { "Page Title" }
		assert_equal "Page Title | #{I18n.t('app_name')}", title
	end

	it "returns app name when page title is missing" do
		assert_equal I18n.t('app_name'), title
	end
end
