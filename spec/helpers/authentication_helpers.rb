def session_cookie(request)
	ActionDispatch::Cookies::CookieJar
		.build(request, cookies.to_hash)
		.encrypted["app_session"]
end

def check_for_user_id_in_session_cookie(user = User.last)
	session = session_cookie(request)
	expect(session).to include("user_id" => user.id)
end

def verify_no_user_id_in_session_cookie
	session = session_cookie(request)
	expect(session).to be_nil
end

def log_in(user)
	post login_path, params: {
		user: {
			email: user.email,
			password: user.password
		}
	}
end

def log_out
	delete logout_path
end

def sign_in_as(user)
	visit login_path
	fill_in('Email', with: user.email)
	fill_in('Password', with: user.password)
	click_on("Log in")
end
