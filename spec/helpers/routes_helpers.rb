def draw_spec_routes(&block)
	Rails.application.routes.disable_clear_and_finalize = true

	Rails.application.routes.draw do
		scope "spec" do
			instance_exec(&block)
		end
	end
end

def reload_routes!
	Rails.application.reload_routes!
end