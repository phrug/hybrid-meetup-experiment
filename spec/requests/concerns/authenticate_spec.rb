require 'rails_helper'

require_relative '../../helpers/routes_helpers'
require_relative '../../helpers/authentication_helpers'

class AuthenticateSpecsController < SpecController
	include Authenticate

	skip_authentication only: [:new, :create]
	allow_unauthenticated only: :show

	def show
		render plain: "User: #{Current.user&.id&.to_s}"
	end
end

RSpec.describe "Sessions", :type => :request do
	let (:user) { create(:user) }

	before do
		draw_spec_routes do
			resource :authenticate_spec,
			only: [:new, :create, :show, :edit]
		end
		user
	end

	after do
		reload_routes!
	end

	it "returns a valid response on requests authenticated by cookie" do
		user.app_sessions.destroy_all
		log_in(user)

		get edit_authenticate_spec_path

		expect(response.status).to eq 200
		assert_match /authenticate_specs#edit/, response.body
	end

	it "renders the login page on unauthenticated request" do
		get edit_authenticate_spec_path

		expect(response.status).to eq 401
		expect(I18n.t("login_required")).to eq flash[:notice]

		expect(response.body).to include "<form"
		expect(response.body).to include "action=\"#{login_path}\""
	end

	it "skips authentication for actions marked to do so" do
		get new_authenticate_spec_path
		expect(response.status).to eq 200
		assert_match /authenticate_specs#new/, response.body

		post authenticate_spec_path
		expect(response.status).to eq 200
		assert_match /authenticate_specs#create/, response.body
	end

	it "allowed unauthenticated requests when marked" do
		get authenticate_spec_path
		expect(response.status).to eq 200
		assert_equal "User: ", response.body

		log_in(user)
		get authenticate_spec_path
		expect(response.status).to eq 200

		assert_equal "User: #{user.id}", response.body
	end
end