require 'rails_helper'

require_relative '../../helpers/authentication_helpers'

class AuthorizeSpecsController < SpecController
	include Authenticate
	include Authorize

	skip_authorization only: :new

	def new
	end

	def show
	end

	private
		def authorizable_resource
		@authorizable = Authorizable.new
	end
end

class Authorizable
	def show?
		user_id = 420
		Current.user == User.find(user_id)
	end
end

RSpec.describe "Authorization", :type => :request do
	let(:user) { create(:user, id: 420) }
	let(:unauthorized_user) { create(:user) }

	before do
		user
		draw_spec_routes do
			resource :authorize_spec, only: [:new, :show]
		end
	end

	after do
		reload_routes!
	end

	it "gives an unauthorized user access to the new action" do
		log_in(unauthorized_user)

		get new_authorize_spec_path

		expect(response.status).to eq 200
		expect(response.body).to match(/authorize_specs#new/)
	end

	it "denies an unauthorized user access to the show action" do
		log_in(unauthorized_user)

		get authorize_spec_path

		expect(response.status).to eq 403
	end

	it "give an authorized user access to the show action" do
		log_in(user)

		get authorize_spec_path

		expect(response.status).to eq 200
		expect(response.body).to match(/authorize_specs#show/)
	end
end