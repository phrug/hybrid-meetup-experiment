require 'rails_helper'
require_relative '../helpers/authentication_helpers'

RSpec.describe "rooms", :type => :request do
    let(:phrug) { create(:phrug) }
    let(:org) { create(:organization) }
    let(:user) { create(:user) }
    let(:admin) { create(:admin) }

  def generate_users
    # create(:membership, user: sunE_manager, organization: sunE)
  end

  describe "unauthenticated users" do
    before do
      generate_users
    end
    it "does what with unauthenticated user?"
  end

  describe "admins" do
    before do
      phrug; org;
      create(:membership, user: admin, organization: phrug)
      log_in admin
    end

    it "can create a room" do
        room_name = "Get a room you guys!"
        post "/rooms", :params => { room: {
        name: room_name,
        scheduled_time: Time.now + 10.days
      },
      organization: phrug
    }

    expect(response).to redirect_to(rooms_path)
    follow_redirect!
    expect(response.body).to include I18n.t('rooms.create.success')
    expect(response.body).to include room_name
    end
  end
end
