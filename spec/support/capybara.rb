Capybara.register_driver :selenium_standalone_chrome do |app|
    Capybara::Selenium::Driver.new(
        app,
        browser: :remote, 
        url: ENV['SELENIUM_HOST'], 
        capabilities: :chrome
    )
end