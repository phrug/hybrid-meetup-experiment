require 'rails_helper'

RSpec.describe AppSession, type: :model do
  it "generates and saves a token when a new record is created" do
    user = create(:user)
    session = user.app_sessions.create

    expect(session.persisted?).to be true
    expect(session.token_digest).not_to be nil
    expect(session.authenticate_token(session.token))
  end
end
