require 'rails_helper'

RSpec.describe User, type: :model do
  let(:org) { create(:organization) }
  let(:user) { create(:user) }

  it "has a valid factory" do
    5.times { create(:user) }
  end

  it "must have a valid email" do
    user.email = ""
    expect(user.valid?).to be false

    user.email = "thisemail.bad"
    expect(user.valid?).to be false

    user.email = "this@emailgood.com"
    expect(user.valid?).to be true
  end

  it "must have a name" do
    expect(user.valid?).to be true

    user.name = ''
# debugger
    expect(user.valid?).to be false
  end

  it "has a password between 8 & the ActiveRecord Max chars" do
    user.password = "passwor"
    
    expect(user.valid?).to be false

  end

  it "strips whitespace from name, email before saving" do 
    user = build(:user)
    user.name = " Jogen Lahomey  "
    user.email = " jogen@anywhere.space "
    user.save
    
    expect(user.name).to eq "Jogen Lahomey"
    expect(user.email).to eq "jogen@anywhere.space"
  end

  it "has only one organization" do
    org2 = create(:organization)

    create(:membership, user: user, organization: org)

    expect(user.organization).to eq org
  end

  it "can create a session with email and correct password" do
    session = User.create_app_session(
      email: user.email,
      password: user.password
    )

    expect(session.nil?).to be false
    expect(session.token.nil?).to be false
  end

  it "cannot create a session with email and incorrect password" do
    session = User.create_app_session(
      email: user.email,
      password: "HINDINAMAN"
    )

    expect(session).to be nil
  end

  it "can create a session with email and correct password" do
    session = User.create_app_session(
      email: "whoami@example.com",
      password: "HINDINAMAN"
    )

    expect(session).to be nil
  end

  it "can authenticate with a valid session id and token" do |variable|
    app_session = user.app_sessions.create
    expect(app_session).to eq(user.authenticate_app_session(app_session.id, app_session.token))
  end

  it "tries to authenticate with a token that doesn't exist and returns false" do
    expect(user.authenticate_app_session(50, "token")).to be_nil
  end

end
