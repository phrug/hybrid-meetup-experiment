require 'rails_helper'

RSpec.describe Room, type: :model do
  it "has a valid factory" do
    org = build(:room, name: 'White Room')
    
    expect(org).to be_valid
  end

  it "rejects new rooms of the same name" do
    create(:room, name: "White Room")
    expect { create(:room, name: "White Room") }.to raise_error(ActiveRecord::RecordInvalid)
  end

    it "rejects new rooms without a name" do
    expect { create(:room, name: nil) }.to raise_error(ActiveRecord::RecordInvalid)
  end
end
