FactoryBot.define do
  factory :organization do
    name { CGI.escapeHTML(Faker::Company.unique.name) }

    factory :phrug do
      name { "PhRUG" }
    end
  end
end
