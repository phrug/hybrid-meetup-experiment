FactoryBot.define do
	factory :user do
    	name { Faker::Name.unique.first_name }
    	email { Faker::Creature::Animal.name.split(" ")[0] + '@' + Faker::Hacker.abbreviation + '.com'  }        
    	password { 'greatpasswordgenghis'}

	    factory :admin do
	    	name { "red" }
	    	password { 'gethimtothegreek' }
	    	after(:create) { |user| user.add_role(:admin) }
		end
	end
end
