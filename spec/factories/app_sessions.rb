FactoryBot.define do
  factory :app_session do
    user { nil }
    token_digest { "MyString" }
  end
end
