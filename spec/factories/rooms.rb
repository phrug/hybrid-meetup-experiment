FactoryBot.define do
  factory :room do
    name { Faker::Movie.title }
    scheduled_time { Time.now + 5.days }
    uuid { SecureRandom.uuid }
    user
  end
end
