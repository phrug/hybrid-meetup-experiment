require 'rails_helper'
require_relative '../helpers/authentication_helpers'

RSpec.describe 'Turbo Frames', type: :feature, js: true do
  let(:phrug) { create(:phrug) }
  let(:admin) { create(:admin) }

  before do
    create(:membership, user: admin, organization: phrug)
  end

  it 'updates the frame without refreshing the page' do
	sign_in_as admin
    visit root_path
    click_on I18n.t("rooms.index.new_button")

    fill_in "Name", with: "No room for you"
    click_on 'Submit'

    click_on 'No room for you'

    click_on 'Enter'

    # sleep(6)
    # it's finding a video selector before the video is rendered
    expect(page).to have_selector('video')
  end
end