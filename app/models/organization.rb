class Organization < ApplicationRecord
  validates :name, uniqueness: true
  validates :name, presence: true
  
  scope :ordered, -> { order(id: :desc) }

  has_many :memberships, dependent: :destroy
  has_many :members, through: :memberships, source: :user
  has_many :rooms, through: :members

end
