class Room < ApplicationRecord
  validates :name, uniqueness: true
  validates :name, presence: true

  include AccessPolicy
    belongs_to :user

   def to_param
     id.to_s
   end
end
