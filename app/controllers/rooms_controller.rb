class RoomsController < ApplicationController
  allow_unauthenticated only: [:index, :show]
  skip_authorization only: [:new, :create]
  before_action :set_room, only: [:edit, :update, :destroy]

  def index

    @org = Organization.first
    if Current.user

      @rooms = Current.user.organization.rooms
    else
      @rooms = @org.rooms
    end
  end

  def new
    @room = Room.new
  end

  def edit
  end

  def create
    room_uuid = SecureRandom.uuid
    @room = Room.new(room_params)
    @room.uuid= room_uuid
    @room.user = Current.user

    if @room.save
      respond_to do |format|
        format.html { redirect_to rooms_path, notice: t('.success') }
        format.turbo_stream
      end
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @room.update(room_params)
      redirect_to rooms_path, notice: "Room was successfully updated."
    else
      render :edit
    end
  end

  def show
    @room = Room.find_by(uuid: params[:id])
    @client = Client.new(id: SecureRandom.uuid) # this is the id the browsers will identify each other as

    cookies.encrypted[:client_id] = @client.id
  end

  def destroy
    @room.destroy

    respond_to do |format|
      format.turbo_stream { render turbo_stream: turbo_stream.remove(@room) }
      format.html { redirect_to rooms_path, notice: t('.success') }
    end
  end

private

  def room_params
    params.require(:room).permit(:name, :scheduled_time)
  end

  def set_room
    @room = Room.find(params[:id])
  end
end
