class UsersController < ApplicationController
  skip_authentication only: [:new, :create]

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    org = Organization.find_by(name: org_params[:organization])
    @user.organization = org

    if @user.save
      Membership.create(user: @user, organization: org)
      @app_session = @user.app_sessions.create
      log_in(@app_session)

      redirect_to root_path,
        status: :see_other,
        flash: { success: t(".welcome", name: @user.name) }
    else
      render :new, status: :unprocessable_entity
    end
  end

private

  def user_params
    params.require(:user).permit(:name, :email, :password)
  end
  def org_params
    params.require(:user).permit(:organization)
  end
end
